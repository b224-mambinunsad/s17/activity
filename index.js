/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
function userDetails(){
	let fullName = prompt("Enter Your Full Name:");
	let age = prompt("Enter Your Age:");
	let location = prompt("Enter Your Address:");
	alert("Thank You For Your Details!");

	console.log("Hello, " + fullName);
	console.log("You are " + age + " years old.");
	console.log("You live in " + location);
};

userDetails();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
function favoriteBands(){
	let bands = ["Kamikazee", "Parokya ni Edgar", "Bamboo", "Bon Jovi", "Beatles"]

	console.log("1. " + bands[0]);
	console.log("2. " + bands[1]);
	console.log("3. " + bands[2]);
	console.log("4. " + bands[3]);
	console.log("5. " + bands[4]);
}

favoriteBands();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
function favoriteMovies(){
	let movies = ["Black Panther", "The Fabelmans", "The Menu", "The Inspection", "She Said"];
	let ratings = ["84%", "95%", "91%", "77%", "86%"]

	console.log("1. " + movies[0]);
	console.log("Rotten Tomatoes Rating: " + ratings[0]);
	console.log("2. " + movies[1]);
	console.log("Rotten Tomatoes Rating: " + ratings[1]);
	console.log("3. " + movies[2]);
	console.log("Rotten Tomatoes Rating: " + ratings[2]);
	console.log("4. " + movies[3]);
	console.log("Rotten Tomatoes Rating: " + ratings[3]);
	console.log("5. " + movies[4]);
	console.log("Rotten Tomatoes Rating: " + ratings[4]);

}

favoriteMovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

// printUsers();
let printFriends = function() {
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};


// console.log(friend1);
// console.log(friend2);

printFriends();